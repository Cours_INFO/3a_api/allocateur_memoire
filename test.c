#include "mem.h"
#include "common.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define NB_TESTS 10

void afficher_zone(void *adresse, size_t taille, int free)
{
  printf("Zone %s, Adresse : %lu, Taille : %lu\n", free?"libre":"occupee",
         adresse - get_memory_adr(), (unsigned long) taille);
}

void TEST_ALLOC_FREE(){
	void* a;

	mem_show(&afficher_zone);
	printf("\n");

	a=mem_alloc(500);			// Allouer l'unique zone occupée
	mem_show(&afficher_zone);
	printf("\n");

	mem_free(a);			// Libéré l'unique zone occupée
	printf("\n");
	mem_show(&afficher_zone);
}

void TEST_FUSION_DROITE(){
	void* a;
	void* b;

	mem_show(&afficher_zone);
	printf("\n");

	a=mem_alloc(500);			// Allouer la première zone occupée
	mem_show(&afficher_zone);
	printf("\n");
	b=mem_alloc(250);			// Allouer la deuxième zone occupée
	mem_show(&afficher_zone);
	printf("\n");

	mem_free(b);			// Libéré la deuxième zone occupée
	mem_show(&afficher_zone);
	printf("\n");
	mem_free(a);			// Libéré la première zone occupée
	mem_show(&afficher_zone);
	printf("\n");
}

void TEST_FUSION_GAUCHE(){
	void* a;
	void* b;
	void* c;

	mem_show(&afficher_zone);
	printf("\n");

	a=mem_alloc(500);			// Allouer la première zone occupée
	mem_show(&afficher_zone);
	printf("\n");
	b=mem_alloc(300);			// Allouer la deuxième zone occupée
	mem_show(&afficher_zone);
	printf("\n");
	c=mem_alloc(100);			// Allouer la troisième zone occupée
	mem_show(&afficher_zone);
	printf("\n");

	mem_free(b);			// Libéré la deuxième zone occupée
	mem_show(&afficher_zone);
	printf("\n");
	mem_free(a);			// Libéré la premième zone occupée
	mem_show(&afficher_zone);
	printf("\n");
	mem_free(c);
}

void TEST_FUSION_DOUBLE(){
	void* a;
	void* b;

	mem_show(&afficher_zone);
	printf("\n");

	a=mem_alloc(500);			// Allouer la première zone occupée
	mem_show(&afficher_zone);
	printf("\n");
	b=mem_alloc(250);			// Allouer la deuxième zone occupée
	mem_show(&afficher_zone);
	printf("\n");

	mem_free(a);			// Libéré la première zone occupée
	mem_show(&afficher_zone);
	printf("\n");
	mem_free(b);			// Libéré la deuxième zone occupée
	mem_show(&afficher_zone);
	printf("\n");
}

void TEST_SATURE(){
	void* a;

	mem_show(&afficher_zone);
	printf("\n");

	a=mem_alloc(159964);			// Allouer l'unique zone occupée
	mem_show(&afficher_zone);
	printf("\n 			Memoire totalement utilisee \n\n");
	
	/*************************** Mémoire totalement utilisée ***************************/
	
	mem_free(a);			// Libéré l'unique zone occupée
	mem_show(&afficher_zone);
	printf("\n");
}

void TEST_SATURE_P(){
	void* a;
	void* b;
	void* c;

	mem_show(&afficher_zone);
	printf("\n");

	a=mem_alloc(45800);			// Allouer la première zone occupée
	mem_show(&afficher_zone);
	printf("\n");
	b=mem_alloc(70000);			// Allouer la deuxième zone occupée
	mem_show(&afficher_zone);
	printf("\n");
	c=mem_alloc(44144);			// Allouer la troisième zone occupée
	mem_show(&afficher_zone);

	printf("\n 			Memoire totalement utilisee \n\n");
	
	/*************************** Mémoire totalement utilisée ***************************/

	mem_free(b);			// Libéré la deuxième zone occupée
	mem_show(&afficher_zone);
	printf("\n");

	mem_free(a);			// Libéré la première zone occupée
	mem_show(&afficher_zone);
	printf("\n");

	mem_free(c);			// Libéré la troisième zone occupée
	mem_show(&afficher_zone);
}

void TEST_BETWEEN(){
	void* a;
	void* b;
	void* c;

	mem_show(&afficher_zone);
	printf("\n");

	a=mem_alloc(1000);			// Allouer la première zone occupée
	mem_show(&afficher_zone);
	printf("\n");
	b=mem_alloc(50);			// Allouer la deuxième zone occupée
	mem_show(&afficher_zone);
	printf("\n");
	c=mem_alloc(32);			// Allouer la troisième zone occupée
	mem_show(&afficher_zone);
	printf("\n");
	
	mem_free(b);				// Libéré la deuxième zone occupée
	mem_show(&afficher_zone);
	printf("\n");

	b=mem_alloc(32);			// Allouer la deuxième zone occupée
	mem_show(&afficher_zone);
	printf("\n");

	mem_free(a);
	mem_free(b);
	mem_free(c);
}

void TEST_POLITIQUE(int fit){
	void* a;
	void* b;
	void* c;
	void* d;

	mem_show(&afficher_zone);
	printf("\n");

	a=mem_alloc(300);			// Allouer la première zone occupée
	mem_show(&afficher_zone);
	printf("\n");
	b=mem_alloc(500);			// Allouer la deuxième zone occupée
	mem_show(&afficher_zone);
	printf("\n");
	c=mem_alloc(2000);			// Allouer la troisième zone occupée
	mem_show(&afficher_zone);
	printf("\n");
	d=mem_alloc(400);			// Allouer la quatrième zone occupée
	mem_show(&afficher_zone);
	printf("\n");
	
	mem_free(c);			// Libéré la première zone occupée
	mem_show(&afficher_zone);
	printf("\n");
	mem_free(a);			// Libéré la troisème zone occupée
	mem_show(&afficher_zone);
	printf("\n");
	
	printf("\ta pour une taille de 1000:\n");
	if(fit==1)				// Allouer dans la Zone avec la plus petit résidu (best)
		printf("\tpour best: a est entre le deuxième et le quatrième\n");
	if(fit==2)
		printf("\tpour worst: a est après le quatième\n");

	a=mem_alloc(1000);
	mem_show(&afficher_zone);
	printf("\n");
	
	mem_free(a);
	mem_free(b);
	mem_free(d);
}

int main(int argc, char *argv[]) {/*
	fprintf(stderr, "Test réalisant de multiples fois une initialisation suivie d'une alloc max.\n"
			"Définir DEBUG à la compilation pour avoir une sortie un peu plus verbeuse."
 		"\n");
	for (int i=0; i<NB_TESTS; i++) {
		debug("Initializing memory\n");
		mem_init(get_memory_adr(), get_memory_size());
		alloc_max(get_memory_size());
	}*/
	mem_init(get_memory_adr(), get_memory_size());

	// TEST OK
	int bTest=1;
	int proto=0;
	while(bTest==1){
		int choix;
		printf("\n****** LISTE DES TESTS ******\n");
		printf("\t1: TEST_ALLOC_FREE\n");
		printf("\t2: TEST_FUSION_DROITE\n");
		printf("\t3: TEST_FUSION_GAUCHE\n");
		printf("\t4: TEST_FUSION_DOUBLE\n");
		printf("\t5: TEST_SATURE avec une allocation\n");
		printf("\t6: TEST_SATURE avec plusieurs allocations\n");
		printf("\t7: TEST_BETWEEN\n");
		printf("\t8: TEST_FIT_BEST (/!\\ mettez &mem_fit_best dans mem.c lg 71)\n");
		printf("\t9: TEST_FIT_WORST (/!\\ mettez &mem_fit_worst dans mem.c lg 71)\n");
		printf("\t0: Pour quitter\n");
		printf("Choisir un test:\n");
		scanf("%d", &choix);
		printf("\n");
		switch(choix){
			case 1:
				TEST_ALLOC_FREE();			//TEST: Allouer / Libérer
				break;
			case 2:
				TEST_FUSION_DROITE();		//TEST: Fusion de Zone libre à droite
				break;
			case 3:
				TEST_FUSION_GAUCHE();		//TEST: Fusion de Zone libre à gauche
				break;
			case 4:
				TEST_FUSION_DOUBLE();		//TEST: Fusion de Zone libre à droite et à gauche
				break;
			case 5:
				TEST_SATURE();				//TEST: Saturer toute la mémoire avec une seule zone
				break;
			case 6:
				TEST_SATURE_P();			//TEST: Saturer toute la mémoire avec plusieurs zones
				break;
			case 7:
				TEST_BETWEEN();				//TEST: Allouer une zone entre 2 Zones Occupées + fit_first
				break;
			case 8:
				if(proto==0){
					TEST_POLITIQUE(1);			//TEST: fit_best quand vous mettez &mem_fit_best dans mem.c lg 71
					proto++;
				}else{
					printf("ATTENTION, vous avez déjâ testé ce test ou TEST_FIT_WORST, veuiller relancer le programme.");
				}
				break;
			case 9:
				if(proto==0){
					TEST_POLITIQUE(2);			//TEST: fit_worst quand vous mettez &mem_fit_worst dans mem.c lg 71
					proto++;
				}else{
					printf("ATTENTION, vous avez déjâ testé ce test ou TEST_FIT_BEST, veuiller relancer le programme.");
				}
				break;
			case 0:
				bTest=0;
				break;
		}
	}
	return 0;
}
