# TP - Allocateur Mémoire

Projet dans le cadre d'un cours à **POLYTECH GRENOBLE**.

## **Les fonctionnalités**
### **Les fonctions principales de l'allocateur**

* `void mem_init(void* mem, size_t taille);:` initialise la liste des zones libres avec une seule zone.
* `void* mem_alloc(size_t size);:` alloue une zone mémoire de taille size et retourne un pointeur sur la dite zone.
* `void mem_free(void *ptr);:` libère une zone mémoire grâce au même pointeur qui est retourné dans la fonction précédente.
* `void mem_show(void (*print)(void *adr, size_t size, int free));:` permet d’afficher l’état de la mémoire.
* `size_t mem_get_size(void *zone);:` retourne la taille maximale que l’utilisateur peut utiliser dans la zone en paramètre.
* `mem_fit_function_t mem_fit_first;:` renvoie l'adresse du premier bloc libre de taille supérieure ou égale à la taille demandée.
* `mem_fit_function_t mem_fit_worst;:` renvoie l'adresse du plus grand bloc libre de taille supérieure ou égale à la taille demandée.
* `mem_fit_function_t mem_fit_best;:` renvoie l'adresse du plus petit bloc libre de taille supérieure ou égale à la taille demandée.

### **Les structures**

* `struct alloue;:` structure utilisée pour les zones occupées.

## **Nos choix d'implantation**

`struct alloue;:` Elle est composée d’uniquement de la taille car, contrairement aux zones libres, les zones occupées n’ont pas besoin de liste. 

Nous avons décidé d’utiliser des structures pour les zones libres et les zones alloué par homogénéité et cohérence et faciliter l’accès au données. 

Nos ZL sont stockés sous la forme d’une liste chaînée plutôt qu’un tableau car la liste est beaucoup plus modulable, de plus le coût d’insertion et de retrait est beaucoup plus faible. 

Nous avons décidé d’utiliser de nombreuses variable locale au sein des fonction afin d’éviter les effets de bord et rendre le code plus lisible en explicitant le rôle de la valeur. 

Nous n'avons pas beaucoup utilisé l’appel de fonction pour faire nos fonctions mem_free et mem_alloc car leur taille est limitée et dispose de quelque cas particulier qui ne peuvent être généralisé (comme le cas où il n’y a plus de ZL). 

## **Les limites de notre code**

Ne vérifie pas si lors du free le paramètre entrée est bien une valeur retournée par un des allocs. 
Si la mémoire est corrompue, notre programme va quand même se lancer au risque d’altérer le reste de la mémoire.

## **Les extensions**

### **Autres politiques**

Nous avons implémenté les politiques **best fit** et **worst fit**.

Best fit permet d’anticiper les grosses allocations mémoire contrairement au worst fit, et même au first fit dans certaines circonstances.

### **Comparaison entre politiques**

La politique **fit first** est utile si on veut faire des allocations rapide mais il créer de la fragmentation et une partie de la mémoire est de la zone libre.

La politique **fit best** est utile si on veut faire des allocations de petit fichier dans ce cas le maximum de mémoire sera optimisé. Pour les autres politiques l’allocation de petit fichier créerait de multiple zone libre de grande taille.

La politique **fit worst** est utile pour des fichiers de tailles moyennes puisqu'elle évite au maximum la fragmentation de la mémoire mais peut laisser de grande zone libre entre des blocs.

## **Les tests que nous avons réalisés**

Nous avons effectué 9 tests pour :
* tester l’**allocation** et la **libération** d’une zone mémoire.
* vérifier que les **fusions de zone libres** sont effectuées (libérer une zone occupée à gauche ou/et à droite d’une zone libre).
* tester d’allouer une zone après avoir **saturé la mémoire totale** (saturation via **un** ou **plusieurs** allocations).
* tester l’allocation entre **deux** zone occupées
* vérifier le bon fonctionnement des politiques **fit_best** et **fit_worst**.